<?php

		header("Access-Control-Allow-Origin: *");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
				
	
							<title>Autostarting (autoplay) videos on iOS and Android 4+</title>	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Autoplay, autostart, video, ios, android" />
<meta name="description" content="Autoplay your videos on iOS and Android 4+ with this substitution to the standard html5 video. Bonus: videos play inline on iPhone 4+" />	<link href="/images/favicon.ico" rel="icon" type="image/x-icon" />
<link href="http://easy-bits.com/css/main-11.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>

	<video data-url="http://s.easy-bits.com/videos/autoplay-videos-ios-android.mp4" width=456 height=360 controls autoplay id="easy-bits-video">
		<source src="http://s.easy-bits.com/videos/autoplay-videos-ios-android.mp4" type="video/mp4" />
	</video>
</body>
<script type="text/javascript" src="http://s.easy-bits.com/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://s.easy-bits.com/js/head.load.min.js"></script>
<script type="text/javascript" src="http://s.easy-bits.com/js/video-flash.js"></script>

<script type="text/javascript" src="http://s.easy-bits.com/js/global-5.js"></script>
</html>
