-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 20, 2015 at 03:25 PM
-- Server version: 5.5.42-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simdesma_dummy`
--

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE IF NOT EXISTS `dashboard` (
  `tahun` text,
  `kd_prov` text,
  `kd_kab` text,
  `kd_kec` text,
  `kd_desa` text,
  `kd_prog` text,
  `kd_keg` text,
  `id_prog` text,
  `kd_rek1` text,
  `kd_rek2` text,
  `kd_rek3` text,
  `kd_rek4` text,
  `kd_rek5` text,
  `kd_kab_gab` text,
  `kd_kec_gab` text,
  `kd_desa_gab` text,
  `kd_prog_gab` text,
  `kd_keg_gab` text,
  `kd_keg_gab1` text,
  `Kd_filter` text,
  `kd_rek1_gab2` text,
  `kd_rek2_gab2` text,
  `kd_rek3_gab2` text,
  `kd_rek4_gab2` text,
  `kd_rek5_gab2` text,
  `kd_rek2_gab1` text,
  `kd_rek3_gab1` text,
  `kd_rek4_gab1` text,
  `kd_rek5_gab1` text,
  `kd_rek1_gab` text,
  `kd_rek2_gab` text,
  `kd_rek3_gab` text,
  `kd_rek4_gab` text,
  `kd_rek5_gab` text,
  `val_rdesa11` text,
  `val_rdesa12` text,
  `val_rdesa6` text,
  `val_rdesa7` text,
  `val_rdesa8` text,
  `val_rdesa9` text,
  `val_rdesa10` text,
  `anggaran` text,
  `realisasi` text,
  `val_tdesa1_2` text,
  `val_tdesa1_3` text,
  `val_rdesa1` text,
  `val_rdesa2` text,
  `val_rdesa3` text,
  `val_rdesa4` text,
  `bulan` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard2`
--

CREATE TABLE IF NOT EXISTS `dashboard2` (
  `tahun` text,
  `kd_prov` text,
  `kd_kab` text,
  `kd_kec` text,
  `kd_desa` text,
  `kd_prog` text,
  `kd_keg` text,
  `id_prog` text,
  `kd_rek1` text,
  `kd_rek2` text,
  `kd_rek3` text,
  `kd_rek4` text,
  `kd_rek5` text,
  `kd_kab_gab` text,
  `kd_kec_gab` text,
  `kd_desa_gab` text,
  `kd_prog_gab` text,
  `kd_keg_gab` text,
  `kd_keg_gab1` text,
  `Kd_filter` text,
  `kd_rek1_gab2` text,
  `kd_rek2_gab2` text,
  `kd_rek3_gab2` text,
  `kd_rek4_gab2` text,
  `kd_rek5_gab2` text,
  `kd_rek2_gab1` text,
  `kd_rek3_gab1` text,
  `kd_rek4_gab1` text,
  `kd_rek5_gab1` text,
  `kd_rek1_gab` text,
  `kd_rek2_gab` text,
  `kd_rek3_gab` text,
  `kd_rek4_gab` text,
  `kd_rek5_gab` text,
  `val_rdesa11` text,
  `val_rdesa12` text,
  `val_rdesa6` text,
  `val_rdesa7` text,
  `val_rdesa8` text,
  `val_rdesa9` text,
  `val_rdesa10` text,
  `anggaran` text,
  `realisasi` text,
  `val_tdesa1_2` text,
  `val_tdesa1_3` text,
  `val_rdesa1` text,
  `val_rdesa2` text,
  `val_rdesa3` text,
  `val_rdesa4` text,
  `bulan` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `level`) VALUES
('Mentarang', 'e4f0b68b66e5424a92d36cf2862ccc03', 'Kecamatan'),
('Malinau Kota', '86ba580b0a805c5fe05a18a9070100ab', 'Kecamatan'),
('Pujungan', '5f938c3fec97d1f63d87ea18ab9c7818', 'Kecamatan'),
('Kayan Hilir', 'cbdacc7e01802f5895c19cf44994df1e', 'Kecamatan'),
('Kayan Hulu', '198fe0def7d68be52735194e037dab88', 'Kecamatan'),
('Malinau Selatan', '1c737d89597941a517fee56e517067fb', 'Kecamatan'),
('Malinau Utara', 'f342cb9f9b33eea4a872fe5e29539219', 'Kecamatan'),
('Malinau Barat', '5a1f74da090412a61e471e8bf214e57e', 'Kecamatan'),
('Sungai Boh', '1ee3e2b90ba2ad26245f4e1b5440a7f3', 'Kecamatan'),
('Kayan Selatan', 'dca5cf3196ee1324a713b9b0a08c4ec4', 'Kecamatan'),
('Bahau Hulu', '31deadc6b32e0f85e7972bb3f0049694', 'Kecamatan'),
('Mentarang Hulu', '31519986117c55852c502c77a25fa05e', 'Kecamatan'),
('Malinau Selatan Hilir', '84994fc5ee7603e650d45323b610c260', 'Kecamatan'),
('Malinau Selatan Hulu', '7facb6bbea1b2507ed020eee55fe1393', 'Kecamatan'),
('Sungai Tubu', '32592a8cd956f299ae72caa065fe849c', 'Kecamatan'),
('HARAPAN MAJU', 'df2db3d2391a5683f5188120380d58ef', 'Desa'),
('LIDUNG KEMENCI', '4d66ff792711bf2801f904b88e6d41b9', 'Desa'),
('LONG BISAI', 'aeb48e3b08f3787be8a9bfbd91581812', 'Desa'),
('LONG GAFID', '33681fdf141e5e82099f66a4d967bc00', 'Desa'),
('LONG LIKU', '468d4c1cd3bc739f3cee74e639a00b60', 'Desa'),
('MENTARANG BARU', '923790354a75fe8a2d1f1c94227e77da', 'Desa'),
('PAKING', '9a97dcd1e7657ec8ca7d9b07267ee9dd', 'Desa'),
('PULAU SAPI', '39f3cd403c7a23bc06e22736d4496124', 'Desa'),
('TEMALANG', 'f77621b04bcc14fbbb37d7cb237e22ad', 'Desa'),
('BATU LIDUNG', 'bc539ad0c4d6f55008a9c793d512b7d4', 'Desa'),
('MALINAU HILIR', '0921f9f3c869da745466445c1ac199ea', 'Desa'),
('MALINAU HULU', 'a39fcd4da9adad82387eff3547414d74', 'Desa'),
('MALINAU KOTA', 'd71d9864589129cb29998cdf40360180', 'Desa'),
('PELITA KANAAN', '90163975bc27e971cd41954c88cc3c8d', 'Desa'),
('TANJUNG KERANJANG', 'cee3d5af71f4fafcb30bf3a48e246e8f', 'Desa'),
('BATU KAJANG', '43c94f9cbae397534920907770033e2c', 'Desa'),
('BILA BEBAYUK', 'f7aa956ffd25f9f1a1324fee48b70652', 'Desa'),
('GONG SOLOK', 'bc650144e9dc8b4f8dee40fabf46f734', 'Desa'),
('LABAN NYARIT', 'a4cf8959ad4e2337eee541609dd534f1', 'Desa'),
('LANGAP', 'c8b57f02954cbd6a5a972b52ccb6f2e1', 'Desa'),
('LONG ADIU', '5ffffd6a285ec1555d2e756f102aa256', 'Desa'),
('LONG LOREH', '37d03d2e53827230dc22d83eadc1eae5', 'Desa'),
('NUNUK TANAH KIBANG', '61456aa275860ca796e4307e74750cff', 'Desa'),
('PAYA SETURAN', 'f994d66d713dab51e5c45105a2ac57e6', 'Desa'),
('PELANCAU', 'ac2a911c3c19814dd9cee5855f6cea28', 'Desa'),
('PUNAN GONG SOLOK', '66e33f556e42755d1cfb7446b728cc75', 'Desa'),
('PUNAN LONG ADIU', '159069efff008e64ad52f684eba27b40', 'Desa'),
('PUNAN RIAN', '841ee6fc1cd7312c4680750e919ce14c', 'Desa'),
('PUNAN SETARAP', '94089194bb0467295164082af019d3a5', 'Desa'),
('SENGAYAN', '81f8aeeb0d5ec6075bb89f699334da75', 'Desa'),
('SETULANG', '764997d2264e3a8a307a807cee1a7fd5', 'Desa'),
('SETARAP', '6edd501baf59bf87ee16489b11f71914', 'Desa'),
('BELAYAN', 'ee7d2a3aba9704df7839d5aa74e71e3e', 'Desa'),
('KALIAMOK', 'b8c2316f2d94eff3a1b9d5a2b727a87f', 'Desa'),
('KELAPIS', '39733a3734e79a29ae99b9f964188bf3', 'Desa'),
('LUBAK MANIS', '0f954e5fa2cf086d1f3cdfc07ca82115', 'Desa'),
('LUSO', '8aeca4dc81e69d202a35b3d9293137a6', 'Desa'),
('MALINAU SEBERANG', '08b82cd7a70d31fe6800ec93d1d1dce2', 'Desa'),
('PUTAT', 'e1dd6f325d2312e3467d4337661f446c', 'Desa'),
('RESPEN TUBU', 'd4b66fbe2144fd7428f25ff50905bbd8', 'Desa'),
('SALAP', '9f53fd93c3fd12629b97da8d04188541', 'Desa'),
('SEMBUAK WAROD', '5dde88e8b98e22e31231f6b408b54657', 'Desa'),
('SERUYUNG', '00795e8a298a7db4d8370fb48214ecd0', 'Desa'),
('SIMENGARIS', 'f8d8e23c9807baba95f4cb957001754a', 'Desa'),
('KUALA LAPANG', 'd3c0aff33d914236f500aedaeb4021f1', 'Desa'),
('LONG BILLA', 'e42cf295569b015d088e42661759b3cf', 'Desa'),
('LONG KENIPE', 'fded9d20cdb52a887e0c1c284ab9e6db', 'Desa'),
('PUNAN BENGALUN', 'f4489d3012417c97a6abf85c22ef5d63', 'Desa'),
('SEMPAYANG', '12168496f0d35f3a0ccbec71a11efe90', 'Desa'),
('SENTABAN', '851719d2da3cc31761f04050d5f26ea0', 'Desa'),
('SESUA', '4f4b277042f8ed5332253fbf9910e7d9', 'Desa'),
('TANJUNG LAPANG', '9cd6dd780908c01569cd7cbcd64a9926', 'Desa'),
('TARAS', '9f5c9c2f06417c68f75b64b945f9157d', 'Desa'),
('bupati', 'c78de339ede23183fc9655b17fd6ba95', 'admin'),
('kadis', 'f984fbd6a856851e26cb3109fba5411f', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `project_requests`
--

CREATE TABLE IF NOT EXISTS `project_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(100) DEFAULT NULL,
  `wordpress` int(11) DEFAULT NULL,
  `codeigniter` int(11) DEFAULT NULL,
  `highcharts` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `project_requests`
--

INSERT INTO `project_requests` (`id`, `month`, `wordpress`, `codeigniter`, `highcharts`) VALUES
(1, 'Jan', 4, 5, 7),
(2, 'Feb', 5, 2, 8),
(3, 'Mar', 6, 3, 9),
(4, 'Apr', 2, 6, 6),
(5, 'May', 5, 7, 7),
(6, 'Jun', 7, 1, 10),
(7, 'Jul', 2, 2, 9),
(8, 'Aug', 1, 6, 7),
(9, 'Sep', 6, 6, 6),
(10, 'Oct', 7, 4, 9),
(11, 'Nov', 3, 6, 8),
(12, 'Dec', 4, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ref_desa1`
--

CREATE TABLE IF NOT EXISTS `ref_desa1` (
  `kd_rdesa1` tinyint(4) NOT NULL,
  `val_rdesa1` varchar(75) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_desa1`
--

INSERT INTO `ref_desa1` (`kd_rdesa1`, `val_rdesa1`) VALUES
(64, 'Provinsi Kalimantan Utara');

-- --------------------------------------------------------

--
-- Table structure for table `ref_desa2`
--

CREATE TABLE IF NOT EXISTS `ref_desa2` (
  `kd_rdesa1` tinyint(4) NOT NULL,
  `kd_rdesa2` tinyint(4) NOT NULL,
  `val_rdesa2` varchar(75) DEFAULT NULL,
  `i_rdesa2` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_desa2`
--

INSERT INTO `ref_desa2` (`kd_rdesa1`, `kd_rdesa2`, `val_rdesa2`, `i_rdesa2`) VALUES
(64, 6, 'Kabupaten Malinau', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_desa3`
--

CREATE TABLE IF NOT EXISTS `ref_desa3` (
  `kd_rdesa1` tinyint(4) NOT NULL,
  `kd_rdesa2` tinyint(4) NOT NULL,
  `kd_rdesa3` tinyint(4) NOT NULL,
  `val_rdesa3` varchar(75) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_desa3`
--

INSERT INTO `ref_desa3` (`kd_rdesa1`, `kd_rdesa2`, `kd_rdesa3`, `val_rdesa3`) VALUES
(64, 6, 1, 'Mentarang'),
(64, 6, 2, 'Malinau Kota'),
(64, 6, 3, 'Pujungan'),
(64, 6, 4, 'Kayan Hilir'),
(64, 6, 5, 'Kayan Hulu'),
(64, 6, 6, 'Malinau Selatan'),
(64, 6, 7, 'Malinau Utara'),
(64, 6, 8, 'Malinau Barat'),
(64, 6, 9, 'Sungai Boh'),
(64, 6, 10, 'Kayan Selatan'),
(64, 6, 11, 'Bahau Hulu'),
(64, 6, 12, 'Mentarang Hulu'),
(64, 6, 13, 'Malinau Selatan Hilir'),
(64, 6, 14, 'Malinau Selatan Hulu'),
(64, 6, 15, 'Sungai Tubu');

-- --------------------------------------------------------

--
-- Table structure for table `ref_desa4`
--

CREATE TABLE IF NOT EXISTS `ref_desa4` (
  `kd_rdesa1` tinyint(4) NOT NULL,
  `kd_rdesa2` tinyint(4) NOT NULL,
  `kd_rdesa3` tinyint(4) NOT NULL,
  `kd_rdesa4` tinyint(4) NOT NULL,
  `val_rdesa4` varchar(75) DEFAULT NULL,
  `i_rdesa4` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_desa4`
--

INSERT INTO `ref_desa4` (`kd_rdesa1`, `kd_rdesa2`, `kd_rdesa3`, `kd_rdesa4`, `val_rdesa4`, `i_rdesa4`) VALUES
(64, 6, 1, 1, 'HARAPAN MAJU', NULL),
(64, 6, 1, 2, 'LIDUNG KEMENCI', NULL),
(64, 6, 1, 3, 'LONG BISAI', NULL),
(64, 6, 1, 4, 'LONG GAFID', NULL),
(64, 6, 1, 5, 'LONG LIKU', NULL),
(64, 6, 1, 6, 'MENTARANG BARU', NULL),
(64, 6, 1, 7, 'PAKING', NULL),
(64, 6, 1, 8, 'PULAU SAPI', NULL),
(64, 6, 1, 9, 'TEMALANG', NULL),
(64, 6, 2, 1, 'BATU LIDUNG', NULL),
(64, 6, 2, 2, 'MALINAU HILIR', NULL),
(64, 6, 2, 3, 'MALINAU HULU', NULL),
(64, 6, 2, 4, 'MALINAU KOTA', NULL),
(64, 6, 2, 5, 'PELITA KANAAN', NULL),
(64, 6, 2, 6, 'TANJUNG KERANJANG', NULL),
(64, 6, 6, 1, 'BATU KAJANG', NULL),
(64, 6, 6, 2, 'BILA BEBAYUK', NULL),
(64, 6, 6, 3, 'GONG SOLOK', NULL),
(64, 6, 6, 5, 'LABAN NYARIT', NULL),
(64, 6, 6, 6, 'LANGAP', NULL),
(64, 6, 6, 7, 'LONG ADIU', NULL),
(64, 6, 6, 10, 'LONG LOREH', NULL),
(64, 6, 6, 14, 'NUNUK TANAH KIBANG', NULL),
(64, 6, 6, 15, 'PAYA SETURAN', NULL),
(64, 6, 6, 16, 'PELANCAU', NULL),
(64, 6, 6, 17, 'PUNAN GONG SOLOK', NULL),
(64, 6, 6, 18, 'PUNAN LONG ADIU', NULL),
(64, 6, 6, 20, 'PUNAN RIAN', NULL),
(64, 6, 6, 21, 'PUNAN SETARAP', NULL),
(64, 6, 6, 22, 'SENGAYAN', NULL),
(64, 6, 6, 23, 'SETULANG', NULL),
(64, 6, 6, 25, 'SETARAP', NULL),
(64, 6, 7, 1, 'BELAYAN', NULL),
(64, 6, 7, 2, 'KALIAMOK', NULL),
(64, 6, 7, 3, 'KELAPIS', NULL),
(64, 6, 7, 4, 'LUBAK MANIS', NULL),
(64, 6, 7, 5, 'LUSO', NULL),
(64, 6, 7, 6, 'MALINAU SEBERANG', NULL),
(64, 6, 7, 7, 'PUTAT', NULL),
(64, 6, 7, 8, 'RESPEN TUBU', NULL),
(64, 6, 7, 9, 'SALAP', NULL),
(64, 6, 7, 10, 'SEMBUAK WAROD', NULL),
(64, 6, 7, 11, 'SERUYUNG', NULL),
(64, 6, 7, 12, 'SIMENGARIS', NULL),
(64, 6, 8, 1, 'KUALA LAPANG', NULL),
(64, 6, 8, 2, 'LONG BILLA', NULL),
(64, 6, 8, 3, 'LONG KENIPE', NULL),
(64, 6, 8, 4, 'PUNAN BENGALUN', NULL),
(64, 6, 8, 5, 'SEMPAYANG', NULL),
(64, 6, 8, 6, 'SENTABAN', NULL),
(64, 6, 8, 7, 'SESUA', NULL),
(64, 6, 8, 8, 'TANJUNG LAPANG', NULL),
(64, 6, 8, 9, 'TARAS', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_marketing`
--

CREATE TABLE IF NOT EXISTS `web_marketing` (
  `name` varchar(50) DEFAULT NULL,
  `val` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_marketing`
--

INSERT INTO `web_marketing` (`name`, `val`) VALUES
('Total Anggaran', 80),
('Realisasi', 20),
('Belanja Modal', 250),
('Realisasi', 100),
('Belanja Non Modal', 100),
('Realisasi', 24);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
