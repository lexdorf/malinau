<?php
session_start();
if(isset($_SESSION['username'])){
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>PEMERINTAH KABUPATEN MALINAU</title>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript">
		function toRp(angka){
		    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
		    var rev2    = '';
		    for(var i = 0; i < rev.length; i++){
		        rev2  += rev[i];
		        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
		            rev2 += '.';
		        }
		    }
		    return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
		}

		$(document).ready(function() {
				now = new Date
			    theYear = now.getYear()
			    if (theYear < 1900)
			    theYear = theYear + 1900;

			var options = {
	            chart: {
	                renderTo: 'container',
	                type: 'column',
	                marginRight: 130,
	                marginBottom: 25
	            },
	            colors: ['#EB021D'],
	            title: {
	                text: 'PEMERINTAH KABUPATEN MALINAU TAHUN '+theYear,
	                x: -20 //center
	            },
	            subtitle: {
	                text: '',
	                x: -20
	            },
	            xAxis: {
	                categories: []
	            },
	            yAxis: {
	                title: {
	                    text: 'Realisasi'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ toRp(this.y);
	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            },
	            series: []
	        }

		$.getJSON("data.php", function(json) {
				options.xAxis.categories = json[0]['data'];
	        	options.series[0] = json[1];
		        chart = new Highcharts.Chart(options);
	        });

		var optionss = {
	            chart: {
	                renderTo: 'container2',
	                type: 'column',
	                marginRight: 130,
	                marginBottom: 25
	            },
	            colors: ['#3C8F1E','#EB021D'],
	            title: {
	                text: '',
	                x: -20 //center
	            },
	            subtitle: {
	                text: 'Anggaran & Realisasi',
	                x: -20
	            },
	            xAxis: {
	                categories: []
	            },
	            yAxis: {
	                title: {
	                    text: 'Anggaran & Realisasi'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ toRp(this.y);
	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            },
	            series: []
	        }

			$.getJSON("data2.php", function(json) {
				optionss.xAxis.categories = json[0]['data'];
	        	optionss.series[0] = json[1];
	        	optionss.series[1] = json[2];
		        chart = new Highcharts.Chart(optionss);
	        });

		var options2 = {
				chart: {
	                renderTo: 'bar1',
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false
	            },
	              colors: ['#3C8F1E','#81E35D'],
	          
	            title: {
	                text: 'Total Anggaran vs Realisasi'
	            },
	            tooltip: {
	                formatter: function() {
	                    return toRp(this.y);
	                }
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: true,
	                        color: 'green',
	                        connectorColor: 'green',
	                        formatter: function() {
	                            return '<b>'+ this.point.name +'</b>';
	                        }
	                    }
	                }
	            },
	            series: [{
	                type: 'pie',
	                name: 'Browser share',
	                data: [],

	            }]
	        }
	        
	        $.getJSON("bar.php", function(json) {
				options2.series[0].data = json;
	        	chart = new Highcharts.Chart(options2);
	        });

		var options3 = {
				chart: {
	                renderTo: 'bar2',
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false
	            },
	            colors: ['#DB2109','#DE513E'],
	            title: {
	                text: 'Belanja Modal  vs Realisasi'
	            },
	            tooltip: {
	                formatter: function() {
	                    return toRp(this.y);
	                }
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: true,
	                        color: 'red',
	                        connectorColor: 'reed',
	                        formatter: function() {
	                            return this.point.name;
	                        }
	                    }
	                }
	            },
	            series: [{
	                type: 'pie',
	                name: 'Browser share',
	                data: []
	            }]
	        }
	        
	        $.getJSON("bar2.php", function(json) {
				options3.series[0].data = json;
	        	chart = new Highcharts.Chart(options3);
	        });
	        

	        var options4 = {
				chart: {
	                renderTo: 'bar3',
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false
	            },
	             colors: ['#BB16C9','#C864D1'],
	            title: {
	                text: 'Belanja Non Modal  vs Realisasi'
	            },
	            tooltip: {
	                formatter: function() {
	                    return toRp(this.y);
	                }
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: true,
	                        color: 'purple',
	                        connectorColor: 'purple',
	                        formatter: function() {
	                            return this.point.name;
	                        }
	                    }
	                }
	            },
	            series: [{
	                type: 'pie',
	                name: 'Browser share',
	                data: []
	            }]
	        }
	        
	        $.getJSON("bar3.php", function(json) {
				options4.series[0].data = json;
	        	chart = new Highcharts.Chart(options4);
	        });
	    });
		</script>
	    <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
	</head>
	<body>
		<div style="float:right">
			<a href="password.php">Ubah Password</a>
			<form method="post" action="kecamatan.php">
			<select name="kecamatan" id="kecamatan">
				<option value="">Cari berdasarkan Kecamatan</option>
				<?php
					include "koneksi.php";	
					$sql	=	"select * from ref_desa3 order by val_rdesa3";
					$query	=	mysql_query($sql);
					while($array=mysql_fetch_array($query)){
						echo "<option value=$array[kd_rdesa3]>$array[val_rdesa3]</option>";
					}
				?>
			</select>
			<input type="submit" value="Cari"/>
			</form>
		</div>
		<br>
		<table width="100%">
			<tr>
			<td width="80%"><div id="container"></div></td>
			<td width="20%"><div id="container2"></div></td>
			</tr>
		</table>
		<br><br><br>
		<div class="content">
		<div class="div1" id="bar1"></div>
		<div class="div2" id="bar2"></div>
		<div class="div3" id="bar3"></div>
	</div>
	<style type="text/css">
	.content{
		width: 1000px;
		margin:0 auto;
	}
	.div1,.div2,.div3{
		padding: 15px;
		border:1px solid #ccc;
		width: 300px;
		float: left;
	}
	</style>

	</body>
</html>
<?php
}else{
	header("location:index.php");
}
?>