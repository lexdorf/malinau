<?php
session_start();
?>
<html>
<head>
<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
 
   $(document).ready(function() {
      /** Membuat Waktu Mulai Hitung Mundur Dengan 
       * var detik = 0,
       * var menit = 1,
       * var jam = 1
       */
       var detik = 3;
       var menit = 0;
       var jam = 0;
 
      /**
       * Membuat function hitung() sebagai Penghitungan Waktu
       */
       function hitung() {
          /** setTimout(hitung, 1000) digunakan untuk 
	   *  mengulang atau merefresh halaman selama 1000 (1 detik) */
	   setTimeout(hitung,1000);
 
	  /** Menampilkan Waktu Timer pada Tag #Timer di HTML yang tersedia */
	   $('#timer').html( 'Sisa Waktu : ' + jam + ' Jam - ' + menit + ' Menit - ' + detik + ' Detik ');
 
	  /** Melakukan Hitung Mundur dengan Mengurangi variabel detik - 1 */
	   detik --;
 
	  /** Jika var detik < 0
	   *  var detik akan dikembalikan ke 59
	   *  Menit akan Berkurang 1
	   */
	   if(detik < 0) {
	      detik = 59;
	      menit --;
 
	      /** Jika menit < 0
	       *  Maka menit akan dikembali ke 59
	       *  Jam akan Berkurang 1
	       */
	       if(menit < 0) {
 		  menit = 59;
		  jam --;
 
		  /** Jika var jam < 0
		   *  clearInterval() Memberhentikan Interval 
		   *  Dan Halaman akan membuka http://tahukahkau.com/
		   */
		   if(jam < 0) { 
                      clearInterval();
 		      window.location = "http://tahukahkau.com";
                   }
	       }
	   } 		
        }
 	/** Menjalankan Function Hitung Waktu Mundur */
        hitung();
   });
// ]]></script>
</head>
<body>
<!--<div id='timer'></div>-->
</body>
</html>
<?php
$login = get_current_user_id();
$_SESSION['login']	=	$login;
$url = explode('/', substr(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),1));


if($login!=""){
$tanggal	=	date("Y-m-d H:i:s");
mysql_query("INSERT INTO `monitor` (`id_artikel`, `id_users`, `tanggal`) VALUES ('$url[0]', '$login', '$tanggal');");
}

// Load parent Editon theme style
function enqueue_parent_theme_style() {
	wp_enqueue_style('animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.1/animate.min.css', array(), '3.2.1', 'all');
	wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0', 'all');
    wp_enqueue_style('parent-style', get_template_directory_uri().'/style.css');
    wp_enqueue_style('fancybox', get_stylesheet_directory_uri().'/vendor/fancybox/2.1.5/jquery.fancybox.css', array(), '2.1.5', 'all');
    wp_enqueue_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), '3.3.2', 'all');
    wp_enqueue_script('jquery', get_stylesheet_directory_uri().'/js/jquery.min.js', array(), "1.11.2", false);
    wp_enqueue_script('jquery-ui', '//code.jquery.com/ui/1.11.2/jquery-ui.js', array('jquery'), '1.11.2', false);
    wp_enqueue_script('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array('jquery'), '3.3.2', false);
    wp_enqueue_script('images-loaded', get_stylesheet_directory_uri().'/vendor/images-loaded/3.1.8/imagesloaded.pkgd.min.js', array(), '3.1.8', false);
    wp_enqueue_script('lazyload', get_stylesheet_directory_uri().'/vendor/lazyload/1.9.3/jquery.lazyload.min.js', array('jquery'), '1.9.3', false);
    wp_enqueue_script('fancybox', get_stylesheet_directory_uri().'/vendor/fancybox/2.1.5/jquery.fancybox.pack.js', array('jquery'), '2.1.5', false);
    wp_enqueue_script('myjs', get_stylesheet_directory_uri().'/js/my.js', array(), null, false);
}
add_action('wp_enqueue_scripts', 'enqueue_parent_theme_style');




// WP_Query() Pagination
if (!function_exists('custom_pagination')) :

	function custom_pagination($query){
		global $paged;
		if ($query->max_num_pages < 2) {
			return;
		}
		$big = 999999999;
		$links = paginate_links(array(
			'base' 			=> str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format' 		=> '?paged=%#%',
			'current' 		=> max(1, $paged),
			'total' 		=> $query->max_num_pages,
			'prev_text' 	=> '<i class="fa fa-angle-left"></i>',
			'next_text' 	=> '<i class="fa fa-angle-right"></i>'
		));

		if ($links) {
		$ret =' 
		<div class="pagination">
			<div class="pagenumbers">'.$links.'</div>
			<div class="divider"></div>
			<div class="clear"></div>
		</div>
		';
		return $ret;
		} else {
			return;
		}
	}

endif;

// Get Post Thumbnail SRC
function get_post_thumb_src($size = 'full'){
	global $post;
	$src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $size);
	return $src[0];
}

// Show Featured Thumbnail in Admin Posts
if (function_exists( 'add_theme_support' )){
    add_filter('manage_posts_columns', 'posts_columns', 5);
    add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 2);
}
function posts_columns($defaults){
    $defaults['wps_post_thumbs'] = __('Image');
    return $defaults;
}
function posts_custom_columns($column_name, $id) {
        if ($column_name === 'wps_post_thumbs') {
        echo the_post_thumbnail(array(35,35));
    }
}


// Hide Admin Bar except Administrator
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}


// Change 'Howdy' in admin var:
// replace WordPress Howdy
function replace_howdy($wp_admin_bar) {
	$my_account=$wp_admin_bar->get_node('my-account');
	$newtitle = str_replace('Howdy,', 'Hello,', $my_account->title);
	$wp_admin_bar->add_node(array(
		'id' => 'my-account',
		'title' => $newtitle,
	));
}
add_filter( 'admin_bar_menu', 'replace_howdy',25 );

add_action('after_setup_theme', 'remove_admin_bar');


get_template_part('shortcode');


// function ag_get_cats_redeclare() {
// 	return 'LOL';
// }
// add_filter('ag_get_cats', 'ag_get_cats_redeclare' );


// Testing Overide Declared Function by APD (Zend Extention)
// override_function('ag_get_cats', '', 'return ag_get_cats_redeclare()');
// function ag_get_cats_redeclare(){
// 	return 'LOL';
// }
// http://php.net/manual/en/function.override-function.php

add_filter('wp_list_categories', 'add_slug_class_wp_list_categories');
function add_slug_class_wp_list_categories($list) {
	$cats = get_categories('hide_empty=0');
		foreach($cats as $cat) {
		$list = str_replace('<a', '<a class="category-'.$cat->term_id.'"', $list );
	}
	return $list;
}

// Get Cat and Order by Parent
function get_cat_orderby_parent(){
	global $post;
	$categories = wp_get_post_categories($post->ID, array('fields' => 'ids'));
	if ($categories) {
	 	$cat_ids 	= implode(',' , $categories);
	 	$cats 		= wp_list_categories('title_li=&style=none&echo=0&include='.$cat_ids);
	 	$cats 		= str_replace('<br />', '', $cats);

	 	// $cats 		= str_replace('<a', '<a class="category-' . $categories[0] . '" ', $cats);
	 	// $ret 		= str_replace('<br />', '', $cats);

	 	// $ret = explode('|', $cats);

	 	// $turn = '';

	 	// $i = 1;
	 	// foreach ($ret as $ret) {
	 	// 	$turn .= str_replace('<a', ' <a class="category-' . $i . '" ', $ret);
	 	// 	$i++;
	 	// }

	 	// return '<span style="color:#000;">'.$cat_ids.'</span><br><span style="color:#000;">'.$turn.'</span>';
	 	return $cats;
	}
}


// Display DB queries, time spent and memory consumption
function performance($visible = false) {
    $stat = sprintf('[%d queries in %.3f seconds, using %.2fMB memory]',
        get_num_queries(),
        timer_stop(0, 3),
        memory_get_peak_usage() / 1024 / 1024
        );
    echo $visible ? $stat : "<!-- {$stat} -->" ;
}
add_action('wp_footer', 'performance', 20);


// I'm desperate hiding a SEO link
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wpseo-menu');

}
// and we hook our function via
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );


// Developer on WORK
function onDebug(){
	$bar = '<div style="padding:10px 0; color:#fff; background:#A60000; text-align:center; font-size:14px; font-weight:bold; width:100%; position:fixed; z-index:9999; bottom:0; box-shadow:0 -4px 4px rgba(0, 0, 0, 0.7);">This page is under development!</div>';
	return $bar;
}

// Disable Metabox for Subscriber:
if (is_admin()) :
	function my_remove_meta_boxes() {
		if (!current_user_can('manage_options')) {
			remove_meta_box('eg-meta-box', 'attachment', 'normal');
			remove_meta_box('wpseo_meta', 'attachment', 'normal');
		}
	}
	add_action('do_meta_boxes', 'my_remove_meta_boxes');
endif;


function custom_register_msg($msg) {
	$pattern = '/Register For This Site/';
	$custom_msg = 'Register for ACT NOW!';
	return preg_replace($pattern, $custom_msg, $msg);
}
add_filter('login_message','custom_register_msg');