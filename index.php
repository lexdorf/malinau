<html> <title>Log In</title>
    <link href="bootstrap.css" rel="stylesheet">
    <link href="stylesheet.css" rel="stylesheet">
    <style>
		body { 
		  -webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
		  background-size: cover;
		}
	</style>
    <body  style="">
	<div class="login-container">
        <div class="login-header bordered">
            <h4>Log In Dashboard Kabupaten Malinau</h4>
        </div>
        <hr>
        <form method="post" action="login.php">
            <div class="login-field">
                <label for="username">Username</label>
                <input type="text" name="username" id="username" placeholder="Username">
                <i class="icon-user"></i>
            </div>
            <div class="login-field">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" placeholder="Password">
                <i class="icon-lock"></i>
            </div>
            <div class="login-button clearfix">
                <button type="submit" class="pull-right btn btn-large blue">MASUK</button>
            </div>
        </form>
    </div>

</body></html>